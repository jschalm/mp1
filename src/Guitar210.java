import java.util.*;

/**
 * @author Jordan Schalm
 *
 * Implements a guitar with multiple strings using the Guitar interface.
 */
public class Guitar210 implements Guitar {
    public static final String KEYBOARD =
        "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ";  // keyboard layout
    private Map<Character, GuitarString> strings;
    private int numTics;
    
    /**
     * Constructor for a Guitar210 object. Initializes as many GuitarString objects
     * as there are characters in KEYBOARD, with frequencies varying such that the 
     * ith string has frequency f = 440 × 2^((i - 24)/12)
     */
    public Guitar210() {
    	strings = new TreeMap<Character, GuitarString>();
    	GuitarString nextString;
    	double frequency;
    	
    	for( int i = 0; i < KEYBOARD.length(); i++ ) {
    		frequency = 440.0 * Math.pow(2.0, (i-24.0)/12.0);
    		nextString = new GuitarString( frequency );
    		strings.put(Character.valueOf(KEYBOARD.charAt(i)),nextString);
    	}
    }
    
    /**
     * Takes an integer value 'pitch' and plucks the string of the corresponding pitch.
     * @param pitch - an integer value between -12 and 24 inclusive.
     */
	@Override
	public void playNote(int pitch) {
		if( pitch + 12 >= 0 && pitch + 12 <= 36) {
			Character key = Character.valueOf(KEYBOARD.charAt(pitch + 12)); // add 12 to pitch to convert a pitch value to an index in the KEYBOARD array
			GuitarString stringToPlay = strings.get(key);
			stringToPlay.pluck();
		}
	}

	/**
	 * @param key - Any character on the keyboard.
	 * @return Returns true if the input char is found in the KEYBOARD string and
	 * false if not.
	 */
	@Override
	public boolean hasString(char key) {
		boolean hasString = false;
		for(int i = 0; i < KEYBOARD.length(); i++) {
			if( KEYBOARD.charAt(i) == key ) {
				hasString = true;
				break;
			}
		}
		return hasString;
	}
	
	/**
	 * Takes a char 'key' as a parameter and plucks the string that corresponds
	 * to that key. If the key has no corresponding string, throws an IllegalArgumentException.
	 * @param key - A character, must be mapped to a string in 'strings'. If not, 
	 * throws an IllegalArgumentException.
	 */
	@Override
	public void pluck(char key) throws IllegalArgumentException {
		if( strings.containsKey(key) ) {
			strings.get(key).pluck();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Samples the front of the ring buffer of each string and returns the sum of samples.
	 */
	@Override
	public double sample() {
		double sum = 0.0;
		for( GuitarString nextString : strings.values() ) {
			sum += nextString.sample();
		}
	return sum;
	}

	/**
	 * Moves every string's ring buffer forward one tic.
	 */
	@Override
	public void tic() {
		GuitarString stringToTic;
		for(int i = 0; i < KEYBOARD.length(); i++) {
			stringToTic = strings.get(KEYBOARD.charAt(i));
			stringToTic.tic();
		}
		numTics++;
	}
	
	/**
	 * Returns the number of times tic() has been called on this instance.
	 */
	@Override
	public int time() {
		return numTics;
	}
}