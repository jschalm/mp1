import java.util.*;

/**
 * @author Jordan Schalm
 *
 * The GuitarString class implements a guitar string with a given frequency.
 */
public class GuitarString {

	private Queue<Double> ringBuffer;
	public static final double ENERGY_DECAY_FACTOR = 0.996;
	
	/**
	 * Standard constructor for GuitarString object. Initializes the ring buffer with zeroes.
	 * 
	 * @param frequency - frequency to determine properties of the GuitarString.
	 * Requires: frequency > 0
	 * 
	 * @throws IllegalArgumentException if frequency is less than 0 or if the ringBuffer object's capacity
	 * will be less than 2.
	 */
	public GuitarString( double frequency ) throws IllegalArgumentException {
		int capacity =  ( int ) Math.round( StdAudio.SAMPLE_RATE / frequency );
		
		try {
			if( frequency <= 0.0 || capacity < 2) {
				throw new IllegalArgumentException();
			}
			else {
				ringBuffer = new LinkedList<Double>();
				for( int i = 0; i < capacity; i++ ) {
					ringBuffer.add( 0.0 );
				} 

			}
		} catch( IllegalArgumentException e ) {
			System.out.println( "The GuitarString could not be initialized. Frequency = " + 
					frequency + ". Capacity = " + capacity + "." );
			throw e;
		}
	}
	
	/**
	 * Constructor for GuitarString object for testing. Initializes ring buffer
	 * with values from input array 'init'.
	 * 
	 * @param init - array of values to be put in the ringBuffer.
	 * Requires: values in array must be on [-0.5, 0.5).
	 * 
	 * @throws IllegalArgumentException if init[] has fewer than 2 values.
	 */
	public GuitarString( double[] init ) throws IllegalArgumentException {
		try {
			if( init.length < 2 ) {
				throw new IllegalArgumentException();
			}
			else {
			ringBuffer = new LinkedList<Double>();
			for( double val : init ) {
					ringBuffer.add( val );
				}
			}
		} catch( IllegalArgumentException e ) {
			System.out.println( "The GuitarString could not be initialized. Array length = " +
					init.length + "." );
			throw e;
		}
	}
	
	/**
	 * Replaces all values in the ringBuffer with random values on [-0.5, 0.5).
	 * Requires: ring buffer must be initialized and non-empty
	 */
	public void pluck() {
		for( int i = 0; i < ringBuffer.size(); i++ ) {
			Double val = Math.random() - 0.5;
			ringBuffer.add( val );
			ringBuffer.remove();
		}
	}
	
	/**
	 * Applies the Karplus-Strong algorithm to the ringBuffer once.
	 * Requires: ringBuffer must be initialized and non-empty.
	 */
	public void tic() {
		double newKarplusStrongVal = ENERGY_DECAY_FACTOR * 0.5 * ( ringBuffer.remove() + ringBuffer.peek() );		
		ringBuffer.add( newKarplusStrongVal );
	}
	
	/**
	 * Samples the first value in the ringBuffer without removing it.
	 * 
	 * @return - returns the first value in the ringBuffer
	 * Requires: ring buffer must be initialized and non-empty
	 */
	public double sample() {
		return ringBuffer.peek();
	}
}
